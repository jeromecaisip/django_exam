from django.contrib import admin

from .forms import AdminBoardForm
from .models import Board


# Register your models here.
class BoardAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'order')

    def get_form(self, request, obj=None, change=False, **kwargs):
        if obj:
            return AdminBoardForm
        else:
            return super().get_form(request, obj, **kwargs)


admin.site.register(Board, BoardAdmin)
