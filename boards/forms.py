from django import forms

from .models import Board, Thread, Post


class AdminBoardForm(forms.ModelForm):
    class Meta:
        model = Board
        fields = ['name', 'creator']

    position = forms.IntegerField(max_value=Board.objects.count() - 1, min_value=0)

    def save(self, commit=True):
        board = super().save(commit=False)
        board.to(self.cleaned_data['position'])
        board.save()
        return board


class BoardForm(forms.ModelForm):
    class Meta:
        model = Board
        fields = ['name']


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['message']


class ThreadForm(forms.ModelForm):
    class Meta:
        model = Thread
        fields = ['title', 'board']

    message = forms.CharField(widget=forms.Textarea, required=True)
