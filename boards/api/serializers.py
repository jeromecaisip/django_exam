from rest_framework import serializers

from ..models import Post, Thread, Board, UserProfile


class PostsSerializer(serializers.ModelSerializer):
    posted_on_board = serializers.ReadOnlyField(source='thread.board.id')
    post_url = serializers.HyperlinkedIdentityField('api:post-detail')

    class Meta:
        model = Post
        fields = ['id', 'message', 'post_url', 'posted_on_board', 'posted_by', 'thread']

    # poster by is a ForeignKeyField here
    posted_by = serializers.ReadOnlyField(source='posted_by.id')


class ThreadSerializer(serializers.ModelSerializer):
    #    posted_on_thread = serializers.ReadOnlyField(source='thread.id')

    #    posted_on_board = serializers.ReadOnlyField(source='thread.board.id')
    posts = serializers.PrimaryKeyRelatedField(many=True, read_only=True, )
    post = serializers.CharField(write_only=True)
    url = serializers.HyperlinkedIdentityField('api:thread-detail')

    class Meta:
        model = Thread
        fields = ['id', 'url', 'board', 'title', 'posts', 'post', 'is_locked']
        read_only_fields = ('is_locked',)


class ForModAndAdminThreadSerializer(ThreadSerializer):
    class Meta:
        model = Thread
        fields = ['id', 'board', 'title', 'posts', 'post', 'is_locked', 'url']


class BoardSerializer(serializers.ModelSerializer):
    #    posted_on_thread = serializers.ReadOnlyField(source='thread.id')

    #    posted_on_board = serializers.ReadOnlyField(source='thread.board.id')
    url = serializers.HyperlinkedIdentityField(view_name='api:board-detail')

    class Meta:
        model = Board
        fields = ['id', 'url', 'creator', 'name']
