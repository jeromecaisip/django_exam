from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from .permissions import IsAdminOrReadOnly, IsOwnerOrReadOnly
from .serializers import PostsSerializer, BoardSerializer, ThreadSerializer, ForModAndAdminThreadSerializer
from ..models import Post, Board, Thread


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostsSerializer
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(posted_by=self.request.user.userprofile)


class BoardViewSet(viewsets.ModelViewSet):
    queryset = Board.objects.all()
    serializer_class = BoardSerializer
    permission_classes = (IsAuthenticated, IsAdminOrReadOnly,)


class ThreadViewSet(viewsets.ModelViewSet):
    queryset = Thread.objects.all()
    serializer_class = ThreadSerializer
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly,)

    def perform_create(self, serializer):
        post = serializer.validated_data.pop('post', None)
        thread = serializer.save(posted_by=self.request.user.userprofile)
        Post.objects.create(message=post, posted_by=self.request.user.userprofile,
                            thread=thread)
        return thread

    def get_serializer_class(self, *args, **kwargs):
        if not self.request.user.userprofile.role == 0:
            return ForModAndAdminThreadSerializer
        else:
            return ThreadSerializer
