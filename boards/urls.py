from django.urls import path
from . import views

app_name = 'boards'

urlpatterns = [
    path('', views.index, name='index'),
    path('thread', views.create_thread, name='new_thread'),
    path('thread/<int:thread_id>', views.thread_view, name='thread_page'),
    path('thread/lock/', views.lock_thread, name='lock_thread'),
    path('board/<int:board_id>', views.board, name='board_page'),
    path('board/new/', views.create_board, name='new_board'),
    path('board/delete/', views.delete_board, name='delete_board'),
]
