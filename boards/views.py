from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, reverse, get_object_or_404

from boards.forms import ThreadForm, PostForm, BoardForm
from boards.models import Board, Thread, Post


# Create your views here.
@login_required()
def index(request):
    boards = Board.objects.order_by('order')

    return render(request, "board/index.html", {'boards': boards})


@login_required()
def create_thread(request):
    if request.method == 'POST':
        form = ThreadForm(data=request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            posted_by = request.user.userprofile
            message = form.cleaned_data['message']
            board = form.cleaned_data['board']
            thread = Thread.objects.create(board=board, title=title, posted_by=posted_by)
            Post.objects.create(posted_by=posted_by, thread=thread, message=message)

        return redirect(reverse('boards:index'))

    form = ThreadForm()
    return render(request, "board/new_thread.html", {'form': form})


@login_required()
def thread_view(request, thread_id):
    thread = get_object_or_404(Thread, pk=thread_id)
    if request.method == 'POST':
        form = PostForm(data=request.POST)
        if form.is_valid():
            Post.objects.create(thread=thread, message=form.cleaned_data['message'], posted_by=request.user.userprofile)

    form = PostForm()
    return render(request, "board/thread_page.html",
                  {'posts': thread.posts.order_by('-created'), 'form': form, 'thread': thread})


@login_required()
def lock_thread(request):
    thread = get_object_or_404(Thread, pk=request.POST.get('thread_id'))
    if request.method == 'POST' and not request.user.userprofile.role == 0:
        if thread.is_locked:
            thread.is_locked = False
        else:
            thread.is_locked = True
        thread.save()
    return redirect(reverse('boards:board_page', args=(thread.board_id,)))


@login_required()
def board(request, board_id):
    board = get_object_or_404(Board, pk=board_id)
    # Note: Solve one-to-many lookups Thread.objects.order_by('posts__created')
    threads = board.sort_threads
    return render(request, "board/board.html",
                  {'board': board, 'threads': threads})


@login_required()
def create_board(request):
    if not request.user.userprofile.role == 2:
        return redirect('/')

    if request.method == 'POST':
        form = BoardForm(data=request.POST)
        if form.is_valid():
            Board.objects.create(name=form.cleaned_data['name'], creator=request.user.userprofile)
            return redirect('/')
        else:
            return render(request, 'board/new_board.html', {'form': form})
    else:
        form = BoardForm()
        return render(request, 'board/new_board.html', {'form': form})


@login_required()
def delete_board(request):
    if not request.user.userprofile.role == 2:
        return redirect('/')

    if request.method == 'POST':
        board_id = request.POST.get('board_id')
        Board.objects.get(id=board_id).delete()

    return redirect(reverse('boards:index'))
