from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.aggregates import Max
from django_extensions.db.models import TimeStampedModel
from ordered_model.models import OrderedModel

from accounts.models import UserProfile
from django.core.exceptions import ValidationError


class Board(OrderedModel, TimeStampedModel):
    name = models.CharField(max_length=255)
    creator = models.ForeignKey(UserProfile, related_name='boards', on_delete=models.CASCADE, limit_choices_to={'role':2})

    def clean(self):
        try:
            if not self.creator.role == 2:
                raise ValidationError({'creator': 'Creator is not an admin.'})
        except:
            pass

    @property
    def position(self):
        return self.order

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def total_posts(self):
        return Post.objects.filter(thread__board=self).count()

    @property
    def sort_threads(self):
        return self.threads.annotate(most_recent=Max('posts__created')).order_by('-most_recent')

    def thread_with_latest_post(self):
        try:
            return self.sort_threads[0]
        except IndexError:
            pass

    def clean(self):
        if not self.creator.role == 2:
            raise ValidationError('Board should be created by admin')

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)


class Thread(TimeStampedModel):
    board = models.ForeignKey(Board, related_name='threads', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    posted_by = models.ForeignKey(UserProfile, related_name='threads', on_delete=models.CASCADE)
    is_locked = models.BooleanField(default=False)

    @property
    def posted_on(self):
        return self.created

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.title

    def get_latest_post(self):
        try:
            return self.posts.order_by('-created')[0]
        except IndexError:
            pass


class Post(TimeStampedModel):
    thread = models.ForeignKey(Thread, related_name='posts', on_delete=models.CASCADE)
    message = models.TextField()
    posted_by = models.ForeignKey(UserProfile, related_name='posts', on_delete=models.CASCADE)

    @property
    def posted_on(self):
        return self.created
