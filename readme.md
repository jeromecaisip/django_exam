## Instructions

Python Version Used: 3.6

Administrator and moderator account can be created via django-admin.


```
pip install -r requirements.txt
python manage.py migrate
```


Note:
Some of existing data used in development are still in the db.sqlite3

To use a fresh db, delete the file and run:
```
python manage.py makemigrations
python manage.py migrate
```