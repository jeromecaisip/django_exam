from django.shortcuts import render
from accounts.models import User,UserProfile
from django.db.models import fields

class BanMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before

        # the view (and later middleware) are called.

        response = self.get_response(request)

        try:
            if not request.user.is_anonymous and not request.user.is_superuser:
                if request.user.userprofile.is_banned:
                    return render(request, 'users/banned.html')
            # Code to be executed for each request/response after
            # the view is called.
        except UserProfile.DoesNotExist:
            pass

        return response



