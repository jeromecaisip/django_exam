from allauth.socialaccount.signals import social_account_added,pre_social_login
from django.dispatch import receiver
from allauth.account.signals import user_signed_up

from .models import UserProfile

@receiver(user_signed_up)
def user_registered(request, user, **kwargs):
    UserProfile.objects.create(user=user)



