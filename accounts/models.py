from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class UserProfile(models.Model):
    class Meta:
        verbose_name = 'Bulletin User'
        verbose_name_plural = 'Bulletin Users'

    poster = 0
    moderator = 1
    administrator = 2
    USER_ROLES = (
        (poster, 'Poster'),
        (moderator, 'Moderator'),
        (administrator, 'Administrator')
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    role = models.IntegerField(choices=USER_ROLES, default=0)
    is_banned = models.BooleanField(default=False)

    def __str__(self):
        return "{0}({1})".format(self.user.username, UserProfile.USER_ROLES[self.role][1])

    def __repr__(self):
        return "{0}({1})".format(self.user.username, UserProfile.USER_ROLES[self.role][1])

    def recent_posts(self):
        return self.posts.order_by('-created')
