from django.contrib import admin
from .models import UserProfile


class UserAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'role')


admin.site.register(UserProfile, UserAdmin)
