from rest_framework import serializers

from ..models import UserProfile


class UserSerializer(serializers.ModelSerializer):
    username = serializers.ReadOnlyField(source='user.username')
    posts = serializers.PrimaryKeyRelatedField(read_only=True, many=True)
    profile_link = serializers.HyperlinkedIdentityField(view_name='api:userprofile-detail')

    class Meta:
        model = UserProfile
        fields = ('id', 'username', 'posts', 'profile_link', 'is_banned')
        read_only_fields = ('is_banned',)


class UserSerializerWithBan(serializers.ModelSerializer):
    username = serializers.ReadOnlyField(source='user.username')
    posts = serializers.PrimaryKeyRelatedField(read_only=True, many=True)
    profile_link = serializers.HyperlinkedIdentityField(view_name='api:userprofile-detail')

    class Meta:
        model = UserProfile
        fields = ('id', 'username', 'posts', 'profile_link', 'is_banned')
