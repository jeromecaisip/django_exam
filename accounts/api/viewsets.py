from rest_framework import mixins
from rest_framework import viewsets

from .serializers import UserSerializer, UserSerializerWithBan
from ..models import UserProfile


class UserModelViewSet(mixins.CreateModelMixin,
                       mixins.RetrieveModelMixin,
                       mixins.UpdateModelMixin,
                       mixins.ListModelMixin,
                       viewsets.GenericViewSet):
    pass


class UserViewset(UserModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserSerializer

    def get_serializer_class(self):
        if not self.request.user.userprofile.role == 0:
            return UserSerializerWithBan
        else:
            return UserSerializer
