from django.conf import settings
from django.contrib.auth import login, views as auth_views
from django.contrib.auth.models import User
from django.shortcuts import render, redirect

from .forms import RegistrationForm
from .models import UserProfile


# Create your views here.

class LoginView(auth_views.LoginView):
    redirect_authenticated_user = True
    template_name = 'users/login.html'


def registration_view(request):
    if not request.user.is_anonymous:
        return redirect('/')

    registration_form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = User.objects.create_user(email=email, password=password, username=username)
            UserProfile.objects.create(user=user)
            login(request, user)
            return redirect(settings.LOGIN_REDIRECT_URL)
        else:
            registration_form = RegistrationForm(request.POST)

    return render(request, 'users/register.html', {'registration_form': registration_form})


def profile_page(request, user_id):
    user = UserProfile.objects.get(id=user_id)
    return render(request, 'users/profile.html', {'user': user})


def ban_user(request):
    if request.method == 'POST' and not request.user.userprofile.role == 0:
        user = UserProfile.objects.get(id=request.POST.get('user_id'))
        if user.is_banned:
            user.is_banned = False
        else:
            user.is_banned = True
        user.save()
    return redirect(request.META.get('HTTP_REFERER', '/'))
