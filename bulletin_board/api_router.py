from rest_framework.routers import DefaultRouter
from accounts.api.viewsets import UserViewset
from boards.api.viewsets import PostViewSet, BoardViewSet, ThreadViewSet

app_name = 'api'
router = DefaultRouter()
router.register('users', UserViewset)
router.register('posts', PostViewSet)
router.register('boards', BoardViewSet)
router.register('threads', ThreadViewSet)

urlpatterns = router.urls
